import DataSource from '../data/data-source.js';

const main = () => {
    const searchElement = document.querySelector("search-bar");
    const mealListElement = document.querySelector("#mealList");

    const onButtonSearchClicked =  () => {
        DataSource.searchMeal(searchElement.value)
        .then(renderResult)
        .catch(fallbackResult)
    };

    const renderResult = results => {
        mealListElement.innerHTML = "";
        results.forEach(meal => {
            const {name, category, area, instruction, thumb} = meal;

            const mealElement = document.createElement("div");
            mealElement.setAttribute("class", "meal");

            mealElement.innerHTML = `<img class="thumb-meal" src="${thumb}" alt="Thumbnail">
                <div class="meal-info">
                <h2>${name}</h2>
                <h5>${category}</h5>
                <h5>${area}</h5>
                <p>${instruction}</p>
                </div>`;
            mealListElement.appendChild(mealElement);
        })
    };

    const fallbackResult = message=> {
        mealListElement.innerHTML = "";
        mealListElement.innerHTML += `<h2 class="placeholder">${message}</h2>`
    };

    searchElement.clickEvent =  onButtonSearchClicked;
};

export default main;